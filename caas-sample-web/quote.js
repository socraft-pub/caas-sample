const QUOTE_ENDPOINT = '/api/quotes/random';

const fetchQuote = async ()=> {
  const emojiEl = document.getElementById('emoji');
  emojiEl.setAttribute('class', 'animate');
  const response = await fetch(QUOTE_ENDPOINT);
  const { author, phrase } = await response.json();
  const authorEl = document.getElementById('author');
  authorEl.innerText = author;
  const phraseEl = document.getElementById('phrase');
  phraseEl.innerText = phrase;
  emojiEl.removeAttribute('class');
};

void fetchQuote();
