package ch.socraft.caas

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

class RandomServiceTest {

    private val randomService = RandomService()

    @Test
    fun `random int should return int between 0 included and 99 excluded`() {
        repeat(1000) {
            val rand = randomService.getRandomInt(99)
            assertTrue(rand >= 0)
            assertTrue(rand < 99)
        }
    }

}
