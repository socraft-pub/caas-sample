# Deploy your app in a container

Each notable step is a commit, so you can follow the sequence easily.


## Building your webapp
![](steps/01.jpeg)

Let's take a standard use case, a webapp that is for now just a back-end with one single REST endpoint.

We'll use Spring Boot, a well-known framework to get you started in no time.

This is an API that returns a random quote from a list of 99 quotes, source here: https://gist.github.com/robatron/a66acc0eed3835119817

I paid no attention to the architecture, this is really basic just to serve our purpose.

Launching it will expose the following route: http://localhost:8080/api/quotes/random


## What about the front-end 
![](steps/02.jpeg)

Consisting of static files, the front-end is going to call the back-end and display data retrieved from the endpoint.

Just like the fron-end, this is a really basic "app" but running in the browser. Opening the file with a browser should call the API and yield a result (i.e. not the Magritte "quote")


## We are DevOps engineers are we not? 
![](steps/03.jpeg)

Let's keep the focus on building something that will be deployable in a repeatable fashion. Something know for ages now.


## Containers!
![](steps/04.jpeg)

To deploy these two components, let's put them in containers.

Spring Boot packages an embedded application server, so let's use that.
```
docker build -t caas-api .
docker run docker run --rm --name caas_api -p 8080:8080 caas-api
```

For the front-end we only need a web server, since it's static files only.
```
docker build -t caas-web .
docker run --rm --name caas_web -p 8888:80 caas
```
That gives us two containers, it's already twice as complicated to deploy.
Docker compose could help up but we don't do that in production do we?

## Two containers, what could go wrong?
![](steps/05.jpeg)

We now have two "domains", one for each container, and without proper routing or configuration, you might end up with the oh so common CORS error. Fortunately we know how to configure this, first try (or not).

## Embedding the front-end
![](steps/06.jpeg)

Here we use a little trick and put the static files directly in the webapp served by the application server.

They will be accessed through `/` while the API will be listening under `/api`.

To embed them, just run the following command in the CI (or manually as we don't have a CI yet)
```
cp -r ../caas-sample-web src/main/resources/static
```
We no longer need the front-end docker image, so only the back-end image needs rebuilding
```
docker build -t caas-webapp .
```
And just like that the CORS issue is gone for good.

## Deployment
![](steps/07.jpeg)

We can now run this single image just like the back-end image
```
docker run docker run --rm --name caas_webapp -p 8080:8080 caas-webapp
```

## Kubernetes?
![](steps/08.jpeg)

You might not want a simple server running `docker run`, a proper orchestrator is more suited for production environments. You might think of Swarm or Kubernetes, but it's probably too much of a hassle for just running a simple webapp.

## Container Runtime...
![](steps/09.jpeg)

In the end, you only need a tool that is going to ensure that your container is running, and maybe handle routing, but that's it.

This simple stateless webapp requires a limited amount of resources, does not need persistent storage, and is only going to run when it is actually queried.

## As a service!
![](steps/10.jpeg)

Why not use a managed service that is going to take care of all that for you? The same way you might want to delegate managing a database to a service, using a managed container runtime is going to make your life easier. The cost is usually flexibility and customization, things that in our situation are really egde cases.

## Cloud Run
![](steps/11.jpeg)

Cloud Run is going to host a container created from an image that you can store in Artifact Registry. This is the easiest way to get started with Cloud Run.

As for the deployment, you can use the (https://cloud.google.com/sdk/gcloud/reference/run)[Google Cloud CLI] `gcloud run deploy` but using a service descriptor is much more explicit and has more options. It looks very much like a Kubernetes Deployment, making the transition easier in the future, should you ever need it.

In addition to the service, Cloud Run requires a policy to explicitly state who is going to have access to the endpoint. This only needs to be run every time the policy changes but for the sake of simplicity let's add it to the pipeline.

## Deployment time
![](steps/13.jpeg)

It boils down to this set of commands to deploy a new revision, using the two provided files:
```
gcloud run services replace caas-sample-service.yml
gcloud run services set-iam-policy caas-sample caas-sample-policy.yml
```

## Hidora
![](steps/14.jpeg)

Hidora is a different beast, it uses jelastic under the hood and I found that using gitlab docker registry is the simplest way to get started.

Publishing the image happens on the same platform as the build, so it's very straightforward. Then it's just a matter of linking the docker image repository to Hidora.

What happens next is very similar to Cloud Run, a container will be spawned based on the image you chose.

## Deployment time
![](steps/16.jpeg)

Contrary to Cloud Run we are deploying using CLI commands, in an imperative instead of a declarative fashion.

You can also use manifests, I have not had the chance to try them out yet but they offer more automation and customization possibilities

## That's it
![](steps/17.jpeg)

You now have a proper deployment process that could run on (almost) any container platform, or maybe just your laptop.
