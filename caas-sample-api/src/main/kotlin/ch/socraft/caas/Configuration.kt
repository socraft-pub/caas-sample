package ch.socraft.caas

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod

@Controller
internal class Routes {
    companion object {
        const val INDEX = "/index.html"
        const val REWRITES =
            "{_:^(?!index\\.html|.*\\.js|.*\\.css|.*\\.jpg|.*\\.png|assets|api|v3|swagger-ui|swagger-resources).*}/**"
    }

    @RequestMapping(
        value = [REWRITES],
        method = [RequestMethod.GET],
    )
    fun index() = INDEX
}
