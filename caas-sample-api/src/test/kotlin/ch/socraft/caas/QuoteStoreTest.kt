package ch.socraft.caas

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

class QuoteStoreTest {

    private val quoteStore = QuoteStore()

    @Test
    fun `quote store should contain 99 quotes`() {
        assertEquals(99, quoteStore.getQuotes().size)
    }

}
