package ch.socraft.caas

import org.springframework.stereotype.Service
import kotlin.random.Random

@Service
class RandomService {
    fun getRandomInt(upperBound: Int = 100) = Random.nextInt(upperBound)
}
