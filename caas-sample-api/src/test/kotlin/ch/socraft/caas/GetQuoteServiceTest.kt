package ch.socraft.caas

import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.junit5.MockKExtension
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith

@ExtendWith(MockKExtension::class)
class GetQuoteServiceTest {
    @MockK lateinit var randomService: RandomService
    val quoteStore = QuoteStore()
    lateinit var getQuoteService: GetQuoteService

    @BeforeEach
    fun setUp() {
        getQuoteService = GetQuoteService(quoteStore, randomService)
    }

    @Test
    fun `first quote should be anonymous`() {
        every {
            randomService.getRandomInt(any())
        } returns 0
        assertEquals("Anonymous", getQuoteService.getOne().author)
    }

    @Test
    fun `last quote should be by Margaret Thatcher`() {
        every {
            randomService.getRandomInt(any())
        } returns 98
        assertEquals("Margaret Thatcher", getQuoteService.getOne().author)
    }
}
