package ch.socraft.caas

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api/quotes")
class QuoteController(
    val getQuoteService: GetQuoteService,
) {
    @GetMapping("/random")
    fun getRandomQuote() = getQuoteService.getOne()
}
