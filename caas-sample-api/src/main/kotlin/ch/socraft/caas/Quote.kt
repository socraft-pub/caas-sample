package ch.socraft.caas

data class Quote(
    val author: String,
    val phrase: String,
)
