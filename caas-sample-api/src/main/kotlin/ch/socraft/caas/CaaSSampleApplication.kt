package ch.socraft.caas

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class CaaSSampleApplication

fun main(args: Array<String>) {
	runApplication<CaaSSampleApplication>(*args)
}
