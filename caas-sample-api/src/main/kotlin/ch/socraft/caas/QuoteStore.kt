package ch.socraft.caas

import org.springframework.stereotype.Service

@Service
@Suppress("MaxLineLength")
class QuoteStore {
    @Suppress("LongMethod")
    fun getQuotes() = listOf(
        Quote(
            author = "Anonymous",
            phrase = "If you want to achieve greatness stop asking for permission.",
        ),
        Quote(
            author = "John Wooden",
            phrase = "Things work out best for those who make the best of how things work out.",
        ),
        Quote(
            author = "Anonymous",
            phrase = "To live a creative life, we must lose our fear of being wrong.",
        ),
        Quote(
            author = "Jim Rohn",
            phrase = "If you are not willing to risk the usual you will have to settle for the ordinary.",
        ),
        Quote(
            author = "Anonymous",
            phrase = "Trust because you are willing to accept the risk, not because it's safe or certain.",
        ),
        Quote(
            author = "Swami Vivekananda",
            phrase = "Take up one idea. Make that one idea your life - think of it, dream of it, live on that idea. Let the brain, muscles, nerves, every part of your body, be full of that idea, and just leave every other idea alone. This is the way to success.",
        ),
        Quote(
            author = "Walt Disney",
            phrase = "All our dreams can come true if we have the courage to pursue them.",
        ),
        Quote(
            author = "Anonymous",
            phrase = "Good things come to people who wait, but better things come to those who go out and get them.",
        ),
        Quote(
            author = "Anonymous",
            phrase = "If you do what you always did, you will get what you always got.",
        ),
        Quote(
            author = "Winston Churchill",
            phrase = "Success is walking from failure to failure with no loss of enthusiasm.",
        ),
        Quote(
            author = "Proverb",
            phrase = "Just when the caterpillar thought the world was ending, he turned into a butterfly.",
        ),
        Quote(
            author = "Anonymous",
            phrase = "Successful entrepreneurs are givers and not takers of positive energy.",
        ),
        Quote(
            author = "Vaibhav Shah",
            phrase = "Whenever you see a successful person you only see the public glories, never the private sacrifices to reach them.",
        ),
        Quote(
            author = "Chris Grosser",
            phrase = "Opportunities don't happen, you create them.",
        ),
        Quote(
            author = "Albert Einstein",
            phrase = "Try not to become a person of success, but rather try to become a person of value.",
        ),
        Quote(
            author = "Eleanor Roosevelt",
            phrase = "Great minds discuss ideas; average minds discuss events; small minds discuss people.",
        ),
        Quote(
            author = "Thomas A. Edison",
            phrase = "I have not failed. I've just found 10,000 ways that won't work.",
        ),
        Quote(
            author = "Kim Garst",
            phrase = "If you don't value your time, neither will others. Stop giving away your time and talents- start charging for it.",
        ),
        Quote(
            author = "David Brinkley",
            phrase = "A successful man is one who can lay a firm foundation with the bricks others have thrown at him.",
        ),
        Quote(
            author = "Eleanor Roosevelt",
            phrase = "No one can make you feel inferior without your consent.",
        ),
        Quote(
            author = "Henry Ford",
            phrase = "The whole secret of a successful life is to find out what is one's destiny to do, and then do it.",
        ),
        Quote(
            author = "Winston Churchill",
            phrase = "If you're going through hell keep going.",
        ),
        Quote(
            author = "Anonymous",
            phrase = "The ones who are crazy enough to think they can change the world, are the ones that do.",
        ),
        Quote(
            author = "Anonymous",
            phrase = "Don't raise your voice, improve your argument.",
        ),
        Quote(
            author = "Oscar Wilde",
            phrase = "What seems to us as bitter trials are often blessings in disguise.",
        ),
        Quote(
            author = "Anonymous",
            phrase = "The meaning of life is to find your gift. The purpose of life is to give it away.",
        ),
        Quote(
            author = "Bruce Feirstein",
            phrase = "The distance between insanity and genius is measured only by success.",
        ),
        Quote(
            author = "Lolly Daskal",
            phrase = "When you stop chasing the wrong things you give the right things a chance to catch you.",
        ),
        Quote(
            author = "John D. Rockefeller",
            phrase = "Don't be afraid to give up the good to go for the great.",
        ),
        Quote(
            author = "Anonymous",
            phrase = "No masterpiece was ever created by a lazy artist.",
        ),
        Quote(
            author = "Nathaniel Hawthorne",
            phrase = "Happiness is a butterfly, which when pursued, is always beyond your grasp, but which, if you will sit down quietly, may alight upon you.",
        ),
        Quote(
            author = "Albert Einstein",
            phrase = "If you can't explain it simply, you don't understand it well enough.",
        ),
        Quote(
            author = "Anonymous",
            phrase = "Blessed are those who can give without remembering and take without forgetting.",
        ),
        Quote(
            author = "Anonymous",
            phrase = "Do one thing every day that scares you.",
        ),
        Quote(
            author = "Anonymous",
            phrase = "What's the point of being alive if you don't at least try to do something remarkable.",
        ),
        Quote(
            author = "Lolly Daskal",
            phrase = "Life is not about finding yourself. Life is about creating yourself.",
        ),
        Quote(
            author = "Anonymous",
            phrase = "Nothing in the world is more common than unsuccessful people with talent.",
        ),
        Quote(
            author = "Anonymous",
            phrase = "Knowledge is being aware of what you can do. Wisdom is knowing when not to do it.",
        ),
        Quote(
            author = "Anonymous",
            phrase = "Your problem isn't the problem. Your reaction is the problem.",
        ),
        Quote(
            author = "Anonymous",
            phrase = "You can do anything, but not everything.",
        ),
        Quote(
            author = "Steve Jobs",
            phrase = "Innovation distinguishes between a leader and a follower.",
        ),
        Quote(
            author = "Ray Goforth",
            phrase = "There are two types of people who will tell you that you cannot make a difference in this world: those who are afraid to try and those who are afraid you will succeed.",
        ),
        Quote(
            author = "Dr. APJ Kalam",
            phrase = "Thinking should become your capital asset, no matter whatever ups and downs you come across in your life.",
        ),
        Quote(
            author = "Thomas Jefferson",
            phrase = "I find that the harder I work, the more luck I seem to have.",
        ),
        Quote(
            author = "Napolean Hill",
            phrase = "The starting point of all achievement is desire.",
        ),
        Quote(
            author = "Robert Collier",
            phrase = "Success is the sum of small efforts, repeated day-in and day-out.",
        ),
        Quote(
            author = "Thomas J. Watson",
            phrase = "If you want to achieve excellence, you can get there today. As of this second, quit doing less-than-excellent work.",
        ),
        Quote(
            author = "Michael John Bobak",
            phrase = "All progress takes place outside the comfort zone.",
        ),
        Quote(
            author = "Philippos",
            phrase = "You may only succeed if you desire succeeding; you may only fail if you do not mind failing.",
        ),
        Quote(
            author = "Mark Twain",
            phrase = "Courage is resistance to fear, mastery of fear - not absense of fear.",
        ),
        Quote(
            author = "Pablo Picasso",
            phrase = "Only put off until tomorrow what you are willing to die having left undone.",
        ),
        Quote(
            author = "Zig Ziglar",
            phrase = "People often say that motivation doesn't last. Well, neither does bathing - that's why we recommend it daily.",
        ),
        Quote(
            author = "Earl Nightingale",
            phrase = "We become what we think about most of the time, and that's the strangest secret.",
        ),
        Quote(
            author = "Vidal Sassoon",
            phrase = "The only place where success comes before work is in the dictionary.",
        ),
        Quote(
            author = "Guy Kawasaki",
            phrase = "The best reason to start an organization is to make meaning; to create a product or service to make the world a better place.",
        ),
        Quote(
            author = "Martha Stewart",
            phrase = "I find that when you have a real interest in life and a curious life, that sleep is not the most important thing.",
        ),
        Quote(
            author = "Anonymous",
            phrase = "It's not what you look at that matters, it's what you see.",
        ),
        Quote(
            author = "Colin R. Davis",
            phrase = "The road to success and the road to failure are almost exactly the same.",
        ),
        Quote(
            author = "Ralph Nader",
            phrase = "The function of leadership is to produce more leaders, not more followers.",
        ),
        Quote(
            author = "Maya Angelou",
            phrase = "Success is liking yourself, liking what you do, and liking how you do it.",
        ),
        Quote(
            author = "Bill Gates",
            phrase = "As we look ahead into the next century, leaders will be those who empower others.",
        ),
        Quote(
            author = "Henry Kravis",
            phrase = "A real entrepreneur is somebody who has no safety net underneath them.",
        ),
        Quote(
            author = "Mark Caine",
            phrase = "The first step toward success is taken when you refuse to be a captive of the environment in which you first find yourself.",
        ),
        Quote(
            author = "Tony Robbins",
            phrase = "People who succeed have momentum. The more they succeed, the more they want to succeed, and the more they find a way to succeed. Similarly, when someone is failing, the tendency is to get on a downward spiral that can even become a self-fulfilling prophecy.",
        ),
        Quote(
            author = "Audre Lorde",
            phrase = "When I dare to be powerful - to use my strength in the service of my vision, then it becomes less and less important whether I am afraid.",
        ),
        Quote(
            author = "Mark Twain",
            phrase = "Whenever you find yourself on the side of the majority, it is time to pause and reflect.",
        ),
        Quote(
            author = "Bruce Lee",
            phrase = "The successful warrior is the average man, with laser-like focus.",
        ),
        Quote(
            author = "Swami Vivekananda",
            phrase = "Take up one idea. Make that one idea your life -- think of it, dream of it, live on that idea. Let the brain, muscles, nerves, every part of your body, be full of that idea, and just leave every other idea alone. This is the way to success.",
        ),
        Quote(
            author = "Dale Carnegie",
            phrase = "Develop success from failures. Discouragement and failure are two of the surest stepping stones to success.",
        ),
        Quote(
            author = "Jim Rohn",
            phrase = "If you don't design your own life plan, chances are you'll fall into someone else's plan. And guess what they have planned for you? Not much.",
        ),
        Quote(
            author = "Gurbaksh Chahal",
            phrase = "If you genuinely want something, don't wait for it -- teach yourself to be impatient.",
        ),
        Quote(
            author = "Robert Kiyosaki",
            phrase = "Don't let the fear of losing be greater than the excitement of winning.",
        ),
        Quote(
            author = "T. Harv Eker",
            phrase = "If you want to make a permanent change, stop focusing on the size of your problems and start focusing on the size of you!",
        ),
        Quote(
            author = "Steve Jobs",
            phrase = "You can't connect the dots looking forward; you can only connect them looking backwards. So you have to trust that the dots will somehow connect in your future. You have to trust in something - your gut, destiny, life, karma, whatever. This approach has never let me down, and it has made all the difference in my life.",
        ),
        Quote(
            author = "Jim Rohn",
            phrase = "Successful people do what unsuccessful people are not willing to do. Don't wish it were easier, wish you were better.",
        ),
        Quote(
            author = "Napoleon Hill",
            phrase = "The number one reason people fail in life is because they listen to their friends, family, and neighbors.",
        ),
        Quote(
            author = "Denis Watiley",
            phrase = "The reason most people never reach their goals is that they don't define them, or ever seriously consider them as believable or achievable. Winners can tell you where they are going, what they plan to do along the way, and who will be sharing the adventure with them.",
        ),
        Quote(
            author = "Jane Smiley",
            phrase = "In my experience, there is only one motivation, and that is desire. No reasons or principle contain it or stand against it.",
        ),
        Quote(
            author = "George Bernard Shaw",
            phrase = "Success does not consist in never making mistakes but in never making the same one a second time.",
        ),
        Quote(
            author = "Diane Ackerman",
            phrase = "I don't want to get to the end of my life and find that I lived just the length of it. I want to have lived the width of it as well.",
        ),
        Quote(
            author = "Michael Jordan",
            phrase = "You must expect great things of yourself before you can do them.",
        ),
        Quote(
            author = "Jim Ryun",
            phrase = "Motivation is what gets you started. Habit is what keeps you going.",
        ),
        Quote(
            author = "Dale Carnegie",
            phrase = "People rarely succeed unless they have fun in what they are doing.",
        ),
        Quote(
            author = "Ella Wheeler Wilcox",
            phrase = "There is no chance, no destiny, no fate, that can hinder or control the firm resolve of a determined soul.",
        ),
        Quote(
            author = "Francis Chan",
            phrase = "Our greatest fear should not be of failure but of succeeding at things in life that don't really matter.",
        ),
        Quote(
            author = "George Lorimer",
            phrase = "You've got to get up every morning with determination if you're going to go to bed with satisfaction.",
        ),
        Quote(
            author = "Mike Gafka",
            phrase = "To be successful you must accept all challenges that come your way. You can't just accept the ones you like.",
        ),
        Quote(
            author = "John C. Maxwell",
            phrase = "Success is...knowing your purpose in life, growing to reach your maximum potential, and sowing seeds that benefit others.",
        ),
        Quote(
            author = "Wayne Dyer",
            phrase = "Be miserable. Or motivate yourself. Whatever has to be done, it's always your choice.",
        ),
        Quote(
            author = "Anatole France",
            phrase = "To accomplish great things, we must not only act, but also dream, not only plan, but also believe.",
        ),
        Quote(
            author = "Dale Carnegie",
            phrase = "Most of the important things in the world have been accomplished by people who have kept on trying when there seemed to be no help at all.",
        ),
        Quote(
            author = "Booker T. Washington",
            phrase = "You measure the size of the accomplishment by the obstacles you had to overcome to reach your goals.",
        ),
        Quote(
            author = "Theodore N. Vail",
            phrase = "Real difficulties can be overcome; it is only the imaginary ones that are unconquerable.",
        ),
        Quote(
            author = "Herman Melville",
            phrase = "It is better to fail in originality than to succeed in imitation.",
        ),
        Quote(
            author = "Virgil",
            phrase = "Fortune sides with him who dares.",
        ),
        Quote(
            author = "Washington Irving",
            phrase = "Little minds are tamed and subdued by misfortune; but great minds rise above it.",
        ),
        Quote(
            author = "Truman Capote",
            phrase = "Failure is the condiment that gives success its flavor.",
        ),
        Quote(
            author = "John R. Wooden",
            phrase = "Don't let what you cannot do interfere with what you can do.",
        ),
        Quote(
            author = "Margaret Thatcher",
            phrase = "You may have to fight a battle more than once to win it.",
        ),
    )
}
