import io.gitlab.arturbosch.detekt.Detekt
import io.gitlab.arturbosch.detekt.DetektCreateBaselineTask
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
	id("org.springframework.boot") version "3.2.5"
	id("io.spring.dependency-management") version "1.1.4"
	kotlin("jvm") version "1.9.23"
	kotlin("plugin.spring") version "1.9.23"
	id("io.gitlab.arturbosch.detekt") version "1.23.6"
}

group = "ch.socraft"
version = "0.0.1-SNAPSHOT"

java {
	sourceCompatibility = JavaVersion.VERSION_21
}

repositories {
	mavenCentral()
}

dependencies {
	implementation("org.springframework.boot:spring-boot-starter-web")
	implementation("org.springframework.boot:spring-boot-starter-actuator")
	implementation("org.jetbrains.kotlin:kotlin-reflect")
	testImplementation("org.springframework.boot:spring-boot-starter-test")
	testImplementation("io.mockk:mockk:1.13.10")
	detektPlugins("io.gitlab.arturbosch.detekt:detekt-formatting:1.23.6")
}

tasks.withType<KotlinCompile> {
	kotlinOptions {
		freeCompilerArgs += "-Xjsr305=strict"
		jvmTarget = "21"
	}
}

tasks.getByName<Jar>("jar") {
	enabled = false
}

tasks.withType<Test> {
	useJUnitPlatform()
}

detekt {
	toolVersion = "1.23.6"
	parallel = true
	autoCorrect = true
	source.setFrom("src/main/kotlin")
	config.setFrom("detekt-config.yml")
	basePath = rootDir.toString()
}

tasks.withType<Detekt>().configureEach {
	jvmTarget = "21"
}

tasks.withType<DetektCreateBaselineTask>().configureEach {
	jvmTarget = "21"
}

tasks.detekt.configure {
	reports {
		// Enable/Disable XML report (default: true)
		xml.required.set(true)
		xml.outputLocation.set(file("build/reports/detekt.xml"))

		// Enable/Disable HTML report (default: true)
		html.required.set(true)
		html.outputLocation.set(file("build/reports/detekt.html"))

		// Enable/Disable TXT report (default: true)
		txt.required.set(true)
		txt.outputLocation.set(file("build/reports/detekt.txt"))

		// Enable/Disable SARIF report (default: false)
		sarif.required.set(true)
		sarif.outputLocation.set(file("build/reports/detekt.sarif"))
	}
}
