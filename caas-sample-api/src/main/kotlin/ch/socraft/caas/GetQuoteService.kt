package ch.socraft.caas

import org.springframework.stereotype.Service

@Service
class GetQuoteService(
    val quoteStore: QuoteStore,
    val randomService: RandomService,
) {

    fun getOne(): Quote {
        val quotes = quoteStore.getQuotes()
        val randomPosition = randomService.getRandomInt(quotes.size)
        return quotes[randomPosition]
    }
}
